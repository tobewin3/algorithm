# algorithm

## 介绍

lc叶良辰的刷题日记。

1. Java、python、cpp的算法模板
2. 题目的分类
3. 题目的代码





## 算法模板

1. dp
   - [背包问题](https://github.com/fuzekun/algorithms/blob/master/%E7%AE%97%E6%B3%95%E6%80%BB%E7%BB%93/dp/%E5%8A%A8%E6%80%81%E8%A7%84%E5%88%92.md)
2.  树
   - [遍历](https://github.com/fuzekun/algorithms/blob/master/%E7%AE%97%E6%B3%95%E6%80%BB%E7%BB%93/%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84/%E6%A0%91.md)
   - [时间戳](https://github.com/fuzekun/algorithms/blob/master/%E7%AE%97%E6%B3%95%E6%80%BB%E7%BB%93/%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84/%E6%A0%91.md)
   - [无根树](https://github.com/fuzekun/algorithms/blob/master/%E7%AE%97%E6%B3%95%E6%80%BB%E7%BB%93/%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84/%E6%A0%91.md)
   - [最近公共祖先](https://github.com/fuzekun/algorithms/blob/master/%E7%AE%97%E6%B3%95%E6%80%BB%E7%BB%93/%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84/%E6%A0%91.md)
3. 字符串
   - [Trie树、KMP、AC自动机](https://github.com/fuzekun/algorithms/blob/master/%E7%AE%97%E6%B3%95%E6%80%BB%E7%BB%93/%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84/Trie%E6%A0%91%20%2B%20AC%E8%87%AA%E5%8A%A8%E6%9C%BA.md)

## 题目分类

1. 基础算法
   - [贪心](https://github.com/fuzekun/algorithms/blob/master/%E7%AE%97%E6%B3%95%E6%80%BB%E7%BB%93/%E5%9F%BA%E7%A1%80%E7%AE%97%E6%B3%95/%E8%B4%AA%E5%BF%83.md)
   - 二分
   - 双指针
   - 前缀和
   - [分治](https://github.com/fuzekun/algorithms/blob/master/%E7%AE%97%E6%B3%95%E6%80%BB%E7%BB%93/%E5%9F%BA%E7%A1%80%E7%AE%97%E6%B3%95/%E5%88%86%E6%B2%BB.md)
2. 数论
3. 图论
4. dp
5. 其他
6. 数据结构
   - [树](https://github.com/fuzekun/algorithms/blob/master/%E7%AE%97%E6%B3%95%E6%80%BB%E7%BB%93/%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84/%E6%A0%91.md)

