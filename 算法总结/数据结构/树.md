# 树



# 题目列表



|                             题目                             |            知识点            | 难度 |
| :----------------------------------------------------------: | :--------------------------: | :--: |
| [6139. 受限条件下可到达节点的数目](https://leetcode.cn/problems/reachable-nodes-with-restrictions/) |  无根树 + 统计子树节点个数   | 中等 |
| [中序遍历的非递归实现](https://leetcode.cn/problems/binary-tree-inorder-traversal/) |             遍历             | 简单 |
|                   [前序遍历的非递归实现]()                   |             遍历             | 简单 |
| [后续遍历的非递归实现](https://leetcode.cn/problems/binary-tree-postorder-traversal/solution/bang-ni-dui-er-cha-shu-bu-zai-mi-mang-che-di-chi-t/) |             遍历             | 简单 |
| [2003. 每棵子树内缺失的最小基因值](https://leetcode.cn/problems/smallest-missing-genetic-value-in-each-subtree/) | 启发式搜索/时间戳/性质 + dfs | 困难 |
| [2322. 从树中删除边的最小分数](https://leetcode.cn/problems/smallest-missing-genetic-value-in-each-subtree/solution/by-man-qian-shu-xiao-ming-ljk8/) |            时间戳            | 困难 |
| [655. 输出二叉树](https://leetcode.cn/problems/print-binary-tree/) |          带区间的树          | 中等 |
| [654. 最大二叉树](https://leetcode.cn/problems/maximum-binary-tree/) |          带区间的树          | 中等 |
| [105. 从前序与中序遍历序列构造二叉树](https://leetcode.cn/problems/construct-binary-tree-from-preorder-and-inorder-traversal/) |          带区间的树          | 中等 |
| [106. 从中序与后序遍历序列构造二叉树](https://leetcode.cn/problems/construct-binary-tree-from-inorder-and-postorder-traversal/) |          带区间的树          | 中等 |
| [889. 根据前序和后序遍历构造二叉树](https://leetcode.cn/problems/construct-binary-tree-from-preorder-and-postorder-traversal/) |          带区间的树          | 中等 |

## 一、树的遍历

### 1. 层次遍历

```cpp

vector<TreeNode *>que;
int cur = 0;
que.push_back(root);
while (cur < que.size()) {
    int n = que.size();
    while (cur < n) {
        int u = que[cur++];
        // 这里写上对根节点对应的操作，比如
        // printf("%d\n", u->val);
        if (u->left) que.push_back(u->left);
        if (u->right) que.push_back(u->right);
    }
}
```



### 2. dfs遍历(略)



### 3. 非递归实现遍历



[中序遍历的非递归实现](https://leetcode.cn/problems/binary-tree-inorder-traversal/)

```cpp
vector<int> inorderTraversal(TreeNode* root) {
        vector<int>ans;
        stack<TreeNode*>s;
        TreeNode *cur = root;
        while (1) {
            while (cur) {
                s.push(cur);
                cur = cur->left;
            }
            if (s.empty()) break;
            cur = s.top(); s.pop();
            ans.push_back(cur->val);
            cur = cur->right;
        }
        return ans;
    }
```



[前序遍历的非递归实现]()

```cpp
 vector<int> preorderTraversal(TreeNode* root) {
     vector<int>ans;
     stack<TreeNode*>s;
     TreeNode *cur = root;
     while (1) {
         while (cur) {
             ans.push_back(cur->val);
             s.push(cur);
             cur = cur->left;
         }
         if (s.empty()) break;
         cur = s.top(); s.pop();
         cur = cur->right;
     }
     return ans;
 }
```



[后续遍历的非递归实现](https://leetcode.cn/problems/binary-tree-postorder-traversal/solution/bang-ni-dui-er-cha-shu-bu-zai-mi-mang-che-di-chi-t/)

- **多了一个pre指针，指向前一个出栈的结点**
- 如果右子树不为空，或者右子树已经遍历完成了，就直接出栈。
- **出栈意味着，cur = NULL。**

```cpp
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int>ans;
        stack<TreeNode *>s;
        TreeNode* cur = root;
        TreeNode *pre = NULL;
        while (1) {
            while (cur) {
                s.push(cur);
                cur = cur->left;
            }
            if (s.empty()) break;
            cur = s.top(); s.pop();
            if (!cur->right || pre == cur->right) {
                  ans.push_back(cur->val);
                  pre = cur;
                  cur = NULL;
            }
            else {
                s.push(cur);
                cur = cur->right;
            }
        }
        return ans;
    }
```



## 二、时间戳操作

- 时间戳：用来判断某个子树是否是另一个结点的子孙结点。或者判断某个结点是否为另一个结点的祖先结点。

- 核心思想：维护时间区间[a, b], 如过区间被包含，说明是子孙结点。

[从树种删除边的最小分数](https://leetcode.cn/problems/minimum-score-after-removals-on-a-tree/)

```cpp
static const int maxn = 1005;
vector<int>G[maxn];
vector<int>nums;
int xr[maxn], in[maxn], out[maxn], cnt = 0;

void dfs(int u, int fa) {   // 无根树转换成有根树，并且使用时间戳
    in[u] = ++cnt;          
    xr[u] = nums[u];
    for(int v : G[u]) {     
        if(fa != v) {
            dfs(v, u);
            xr[u] ^= xr[v];
        }
    }
    out[u] = cnt;
}

void dfs(int u) {			// 有根树使用时间戳。
    int[u] = ++cnt;
    for (int v : G[u]) dfs(v);
    out[u] = cnt;
}

bool isAncestor(int i, int j) {
    return in[i] < in[j] && in[j] <= out[i];	// 如果取等号，那么就说明这个
}

```

从0开始，**要么j是i的祖先，要么i是j的祖先。要么互相不为祖先**。三种情况。



题目列表

- #### [2003. 每棵子树内缺失的最小基因值](https://leetcode.cn/problems/smallest-missing-genetic-value-in-each-subtree/)

- #### [2322. 从树中删除边的最小分数](https://leetcode.cn/problems/minimum-score-after-removals-on-a-tree/)





## 三、无根树转换成有根树





```python
def dfs(u, fa) :
    for (v in G[u]) :
        if (fa != v)
            pa[v] = u
            dfs(v, u)
            # 统计操作
            
```



```cpp
void dfs(int u, int fa) {   
    
    for(int v : G[u]) {     
        if(fa != v) {
            pa[v] = u;
            dfs(v, u);
            // 统计操作
        }
    }
}

```

## 四、倍增找最近公共祖先



## 五、 带区间的树
