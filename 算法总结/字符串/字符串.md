# 字符串

## KMP算法



nx[j]:保存p[0~j -1]这个字符串中最长的相同的前后缀长度 + 1。

1. 为什么要 + 1,因为nx[j]实际是 : j匹配失败时候，**下一个**需要匹配的, 前后缀相同, 那么下一个就是 + 1。
2. 思想是最长的前后缀相同的话，那么 + 1就是下一个需要进行匹配的。



当p[k] == p[j]时候，理所应该nx[++j] = k++;

当不等于的时候，就得回溯k = nx[k],直到k等于-1或者p[k] == p[j]的时候，那么p[0~k] == p[j - k, j] 所以nx[j++] = k++;

当p[++j] = p[++k]的时候，就不用进行匹配了，因为匹配了还是得回到nx[k]，所以不如直接递归式的回到。



```cpp

#include <iostream>

using namespace std;

const int N = 1e5 + 10, M = 1e6 + 10;
char p[N], s[M];
int nx[N];

int main()
{
    int n, m;
    cin >> n >> p >> m >> s;
    
    int j = 0, k = -1, i = 0;
    nx[j] = -1;
    while(j < n) {
        if (k == -1 || p[j] == p[k]) {
            if (p[++j] == p[++k]) nx[j] = nx[k];
            else nx[j] = k;
        } else k = nx[k];
    }
    
    j = 0;
    while(i < m) {
        if (j == -1 || s[i] == p[j]) i++, j++;
        else j = nx[j];
        if (j == n) {
            cout << i - j << ' ' ;
            j = nx[j];
        }
    }
    return 0;
}

```

