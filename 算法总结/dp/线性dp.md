# 线性dp

## 题目

|                             题目                             |            知识点             | 难度 |
| :----------------------------------------------------------: | :---------------------------: | :--: |
| [6137. 检查数组是否存在有效划分](https://leetcode.cn/problems/check-if-there-is-a-valid-partition-for-the-array/solution/jian-dan-dp-by-man-qian-shu-xiao-ming-0w2g/) |              dp               | 中等 |
| [6138. 最长理想子序列](https://leetcode.cn/problems/longest-ideal-subsequence/) |              dp               | 中等 |
| [646. 最长数对链](https://leetcode.cn/problems/maximum-length-of-pair-chain/) |      最长上升子序列模型       | 中等 |
| [354. 俄罗斯套娃信封问题](https://leetcode.cn/problems/russian-doll-envelopes/) | 最长上升子序列模型 + 逆序排序 | 困难 |



## 具体讲解

1. 354 题中的对于第二维逆序排序后，那么相等的第一维，只能选择一个。