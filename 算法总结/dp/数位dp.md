# 数位dp

|                             题目                             |    知识点    | 难度 |
| :----------------------------------------------------------: | :----------: | :--: |
| [2376. 统计特殊整数](https://leetcode.cn/problems/count-special-integers/) |  数位dp模板  | 困难 |
| [600. 不含连续1的非负整数](https://leetcode.cn/problems/non-negative-integers-without-consecutive-ones/solution/shu-wei-dp-by-man-qian-shu-xiao-ming-felv/) |    数位dp    | 困难 |
|                     [233. 数字1的个数]()                     | 数位dp模板题 | 困难 |
|                                                              |              |      |
|                                                              |              |      |
|                                                              |              |      |
|                                                              |              |      |
|                                                              |              |      |
|                                                              |              |      |