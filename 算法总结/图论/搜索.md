# 搜索



## 题目

| 题目                                                         | 知识点                            | 难度 |
| ------------------------------------------------------------ | --------------------------------- | ---- |
| [847. 访问所有节点的最短路径](https://leetcode.cn/problems/shortest-path-visiting-all-nodes/solution/gong-shui-san-xie-yi-ti-shuang-jie-bfs-z-6p2k/) | bfs + 状压 / astar / floyd + 状压 | 困难 |
| [1654. 到家的最少跳跃次数](https://leetcode.cn/problems/minimum-jumps-to-reach-home/) | bfs                               | 中等 |
|                                                              |                                   |      |
|                                                              |                                   |      |
|                                                              |                                   |      |
|                                                              |                                   |      |



## bfs

### 代码模板



## dfs

### 代码模板

